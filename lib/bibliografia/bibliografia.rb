
class Bibliografia
    attr_reader :m_autores , :m_titulo , :m_serie , :m_editorial , :m_num_edicion , :m_fechaPublicacion , :m_isbn 
   
    #Constructor con todos los parámetros
    def initialize(a_autores,a_titulo,a_num_edicion,a_editorial,a_fechaPublicacion,a_isbn)
       raise TypeError, "Error, falta autor." if a_autores.equal?""
       raise TypeError, "Error, falta el número de edición" if a_num_edicion.equal?nil
       raise TypeError, "Error, falta el número de edición" if a_fechaPublicacion.equal?""
       raise TypeError, "Error, falta el número de edición" if a_isbn.equal?""

       @m_autores = a_autores
       @m_titulo = a_titulo
       @m_num_edicion = a_num_edicion
       @m_editorial = a_editorial
       @m_fechaPublicacion = a_fechaPublicacion
       @m_isbn = a_isbn
    end
    
    #Devuelve el listado de autores
    def f_getObtenerListadoAutores
        @m_autores
    end
    
    #Devuelve el titulo
    def f_getObtenerTitulo
        @m_titulo
    end
    
    #Se asigna una serie a la variable Serie
    def f_setSerie(a_serie)
        @m_serie = a_serie
    end
    
    #Devuelve la editorial
    def f_getObtenerEditorial
        @m_editorial
    end
    
    #Devuelve el numero de edicion
    def f_getObtenerNumeroEdicion
        @m_num_edicion
    end
    
    #Devuelve el listado de los ISBN
    def f_getObtenerListadoIsbn
        @m_isbn
    end
    
    #Devuelve la fecha de publicación
    def f_getObtenerFechaPublicacion
        @m_fechaPublicacion
    end
    
    #Devuelve la referencia formateada
    def to_s
        "#{ @m_autores} #{@m_titulo} #{@m_serie} #{@m_editorial} #{@m_isbn}"
    end
    
end

#libro = Bibliografia.new(["Dave Thomas", "Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Programatic Programers Guide","4","Progmatic Bookshelf","(July 7, 2013","ISBN-13: 978-1937785499,,ISBN-10: 1937785491")