require "spec_helper"
require "./lib/bibliografia/bibliografia.rb"

describe Bibliografia do
	before :each do
    	@libro = Bibliografia.new(["Dave Thomas"],"Programming Ruby 1.9 & 2.0: The Programatic Programers Guide","4 edition","Progmatic Bookshelf","July 7, 2013","ISBN-13: 978-1937785499,,ISBN-10: 1937785491")
		@libro.f_setSerie("Informática")
  	end

	it "Debe existir uno o mas autores" do
		@libro.m_titulo != ""
	end

	it "Debe Existir un Título." do
		@libro.m_titulo != ""
	end

	it "Debe existir o no una serie" do
		@libro.m_serie != ""
	end

	it "Debe existir una editorial." do
		@libro.m_editorial != ""
	end

	it "Debe existir un número de edición." do
		@libro.m_num_edicion != ""
	end

	it "Debe existir una fecha de publicación." do
		@libro.m_fechaPublicacion != ""
	end

	it " Debe existir uno o más números ISBN." do
		@libro.m_isbn != ""
	end

	it "Existe un método para obtener el listado de autores." do
		@libro.f_getObtenerListadoAutores
	end

	it "Existe un método para obtener el título." do
		@libro.f_getObtenerTitulo
	end

	it "Existe un método para obtener la editorial." do
		@libro.f_getObtenerEditorial
	end

	it "Existe un método para obtener el número de edición" do
		@libro.f_getObtenerNumeroEdicion
	end

	it "Existe un método para obtener la fecha de publicación." do
		@libro.f_getObtenerFechaPublicacion
	end

	it "Existe un método para obtener el listado de ISBN" do
		@libro.f_getObtenerListadoIsbn
	end

	it " Existe un método para obtener la referencia formateada." do
		@libro.to_s
	end

end
